import {Component} from '@angular/core';
import {DataService} from "../service/data.service";
import {Pax} from "../pax";

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
 
    dark = false;

    constructor(public data: DataService) {
        this.loadTheme()

        this.data.prefersColor.addEventListener(
            'change',
            mediaQuery => {
                this.dark = mediaQuery.matches;
                this.updateDarkMode();
            }
          );
    }

    async loadTheme() {
        await this.waitFor(_ => this.data.dataRetrieved === true);
        this.dark = this.data.darkTheme;
        if (this.dark) {
            console.log("Load dark theme")
        }
        this.updateDarkMode();
    }

    waitFor(conditionFunction) {
        const poll = resolve => {
          if(conditionFunction()) resolve();
          else setTimeout(_ => poll(resolve), 400);
        }
        return new Promise(poll);
    }

    updateDarkMode() {
        this.data.darkTheme = this.dark;
        document.body.classList.toggle('dark', this.dark);
        if (this.data.dataRetrieved) { this.data.saveData(); }
    }

    addPax() {
        if (this.data.listePax.length > 0) {
            this.data.listePax.push(
                new Pax(
                    this.data.listePax[0].adresse,
                    this.data.listePax[0].ville,
                    this.data.listePax[0].cp
                ));
        } else {
            this.data.listePax.push(new Pax());
        }

        this.data.saveData();
    }

    delPax(firstname) {
        if (firstname.length > 0) {
            this.data.listePax = this.data.listePax.filter(function(el) { return el.prenom != firstname; });
        } else {
            this.data.listePax = this.data.listePax.filter(function(el) { return el.prenom != ""; });
        }

        this.data.saveData();
    }

}