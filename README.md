# Covid_french_attestation
> Attestation of travel during lockdown in France

![](resources/android/icon/drawable-xxhdpi-icon.png)

Designed to create an easy and local certificate of movement for periods of lockdown in France.

This project is a fork of this [repository](https://github.com/WinstonHill42/gen_attestation) which has already done an awesome job and to which I've added some features.

## Context

This application has been created to work without connection and is based on the official form. It is an offline alternative to the TousAntiCovid application launched by the French Government on October 22, 2020.


Priority has been given to **privacy**. Indeed, the application runs locally on the phone and does not contact any server to avoid activity disclosures via ip and cookie logs. The data is stored only on the phone to facilitate the generation of new certificates and is easily erasable via the "Reset" buttons.

The ability to select multiple reasons is not a bug, it is a feature found on the official application. It corresponds to an official recommendation of the Ministry of the Interior readable on their website :
> Vous êtes incités à limiter vos sorties aux cas limitatifs énumérés, il est donc conseillé de grouper vos sorties et il est donc possible d’indiquer plusieurs motifs.

Because there is no insurance without transparency, the source code is of course available here in gitlab.

Don't hesitate to bring up ideas for improvement!

## How to compile the app ?
IONIC project (angular / typescript / cordova)

To compile this application you just need to install [IONIC](https://ionicframework.com/docs/intro/cli).
As well as the software stack corresponding to your destination platform (android or iOS)

Once it's done, download sources :
```sh
git clone https://gitlab.com/A1CY0N/covid_french_attestation.git
```
And run following commands to genereate the apk :
```sh
ionic cordova platform rm android --verbose
ionic cordova platform add android --verbose
ionic cordova build android --prod --release --verbose
``` 
You should have a ```app-release-unsigned.apk```file.

If you want to install your application on your phone, you need to sign your APK file. To do this, you need to create a new certificate/store key.

```sh
keytool -genkey -v -keystore my-private-release-key.jks -keyalg RSA -keysize 4096 -validity 10000 -alias my-alias
```
And then you can sign your apk using jarsigner in the JDK :

```sh
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-private-release-key.jks app-release-unsigned.apk my-alias
```
Finally, the apk can be optimised by running :
```sh
zipalign -v 4 app-release-unsigned.apk myAwesomeApp.apk
```
## Application overview
### Classic theme
![Main Menu](resources/screenshots/0.png "Main menu")
![Date selection](resources/screenshots/1.png "Date selection")
![Personal information backup](resources/screenshots/2.png "Personal information backup")
![Family](resources/screenshots/3.png "Family")
![Generation of an attestation](resources/screenshots/4.png "Generation of an attestation")
![List of attestations](resources/screenshots/5.png "List of attestations")
![Delete attestations](resources/screenshots/16.png "Delete all attestations")
![QR codes](resources/screenshots/6.png "QR codes")
![About](resources/screenshots/7.png "About")

### Dark theme
![Main Menu](resources/screenshots/18.png "Main menu")
![Date selection](resources/screenshots/17.png "Date selection")
![Family](resources/screenshots/8.png "Family")
![Generation of an attestation](resources/screenshots/9.png "Generation of an attestation")
![List of attestations](resources/screenshots/10.png "List of attestations")
![Delete attestations](resources/screenshots/15.png "Delete all attestations")
![QR codes](resources/screenshots/11.png "QR codes")
![About](resources/screenshots/12.png "About")
## Generated attestations
![PDF](resources/screenshots/13.png "PDF")
![Funny pdf](resources/screenshots/14.png "Funny pdf")

## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

[@WinsonHill42](https://github.com/WinstonHill42) – devmobile462@gmail.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/covid_french_attestation>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request